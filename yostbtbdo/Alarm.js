/**
 * A sensor-based alarm that detects detonations within its radius.
 * @param game Parent game
 * @param position {x||1,y||1,radius||1,timeout||10000}
 */
var Alarm = function(game, data) {
	this.data = data ||{};
	this.position = new b2Vec2(this.data.x || 1, this.data.y || 1);
	this.radius = this.data.radius || 1;
	this.timeout = this.data.timeout || 10000;
	this.game = game;
	this.world = this.game.world;
	
	this.isAlarmTriggered = false;
	this.isAlarmRinging = false;
	
	// Draw alarm on floor
	this.bodyDefinition = new b2BodyDef();
	this.fixtureDefinition = new b2FixtureDef();
	var circleShape = new b2CircleShape();
	circleShape.m_radius = 0.10;
	
	this.bodyDefinition.type = b2Body.b2_staticBody;
	this.bodyDefinition.position = this.position;
	this.bodyDefinition.userData = this;
	this.fixtureDefinition.isSensor = true;
	this.fixtureDefinition.shape = circleShape;
	this.fixtureDefinition.filter.categoryBits = this.world.masks.ALARM;
	this.fixtureDefinition.filter.maskBits = 
		this.world.masks.PLAYER | 
		this.world.masks.BLAST;
	this.body = this.world.CreateBody(this.bodyDefinition);
	this.body.CreateFixture(this.fixtureDefinition);
	
	// add in sensor fixture
	this.sensorFixtureDefinition = new b2FixtureDef();
	var sensorCircleShape = new b2CircleShape();
	sensorCircleShape.m_radius = this.radius;
	this.sensorFixtureDefinition.shape = sensorCircleShape;
	this.sensorFixtureDefinition.isSensor = true;
	this.sensorFixtureDefinition.filter.categoryBits = this.world.masks.ALARM;
	this.sensorFixtureDefinition.filter.maskBits = 
		this.world.masks.PLAYER | 
		this.world.masks.BLAST;
	this.body.CreateFixture(this.sensorFixtureDefinition);
};

/**
 * Handle contacts for this sensor
 */
Alarm.prototype.contact = function(contact) {
	
	this.isAlarmTriggered = true;	
    
};

/**
 * Start this alarm ringing!
 */
Alarm.prototype.ring = function() {
	if (!this.isAlarmRinging) {
		this.isAlarmRinging = true;
		console.log("Alarm has been set off!");
		var that = this;
		setTimeout(function(){that.reset();},this.timeout);
	}
};

/**
 * Reset the alarm
 */
Alarm.prototype.reset = function() {
	this.isAlarmTriggered = false;
	this.isAlarmRinging = false;
	console.log("Alarm stopped ringing");
};

/**
 * Update this alarm
 */
Alarm.prototype.update = function() {
	if (this.isAlarmTriggered) {
		this.ring();
	}
};