/**
 * A goal object
 * @param game Parent game
 * @data data {x||1,y||1}
 */
var Goal = function(game, data) {
	this.data = data || {};
	this.position = new b2Vec2(this.data.x || 1, this.data.y || 1);
	this.game = game;
	this.world = this.game.world;
	
	this.bodyDefinition = new b2BodyDef();
	this.fixtureDefinition = new b2FixtureDef();
	var circleShape = new b2CircleShape();
	circleShape.m_radius = 0.10;
	
	this.bodyDefinition.type = b2Body.b2_staticBody;
	this.bodyDefinition.position = this.position;
	this.bodyDefinition.userData = this;
	this.fixtureDefinition.isSensor = true;
	this.fixtureDefinition.shape = circleShape;
	this.fixtureDefinition.filter.categoryBits = this.world.masks.GOAL;
	this.fixtureDefinition.filter.maskBits = this.world.masks.PLAYER;
	this.body = this.world.CreateBody(this.bodyDefinition);
	this.body.CreateFixture(this.fixtureDefinition);
	
};

/**
 * Handle contacts for this goal
 */
Goal.prototype.contact = function(contact) {
	
	console.log("Goal reached");	
    
};
