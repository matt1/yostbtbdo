var Game = function() {
	// Based on examples at http://buildnewgames.com/box2dweb/
};

/**
 * Initialise the game engine
 * 
 * @param canvas Canvas element to draw game on
 */
Game.prototype.init = function(canvas) {
	
	window.addEventListener("keydown", this.handleInput, true);
	
	this.initBox2D(canvas);
	this.debugDraw();
	this.initWorld();
};

/**
 * Load map data
 */
Game.prototype.loadMap = function(mapData) {
	this.bodies.push(new Body(this, {x:3,y:3}));
	this.bodies.push(new Body(this, {x:4,y:3}));
	this.bodies.push(new Body(this, {x:5,y:3}));
	this.bodies.push(new Body(this, {x:6,y:3}));
	this.bodies.push(new Body(this, {x:7,y:3}));
	this.bodies.push(new Body(this, {x:8,y:3}));
	this.bodies.push(new Body(this, {x:9,y:3}));
	
	this.bodies.push(new Body(this, {x:3,y:4}));
	this.bodies.push(new Body(this, {x:4,y:4}));
	this.bodies.push(new Body(this, {x:5,y:4}));
	this.bodies.push(new Body(this, {x:6,y:4}));
	this.bodies.push(new Body(this, {x:7,y:4}));
	this.bodies.push(new Body(this, {x:8,y:4}));
	this.bodies.push(new Body(this, {x:9,y:4}));
	
	this.bodies.push(new Body(this, {x:8,y:5}));
	this.bodies.push(new Body(this, {x:9,y:5}));
	
	this.bodies.push(new Body(this, {x:3,y:6}));
	this.bodies.push(new Body(this, {x:4,y:6}));
	this.bodies.push(new Body(this, {x:5,y:6}));
	this.bodies.push(new Body(this, {x:6,y:6}));
	
	this.bodies.push(new Body(this, {x:8,y:6}));
	this.bodies.push(new Body(this, {x:9,y:6}));

	this.bodies.push(new Body(this, {x:3,y:7}));
	this.bodies.push(new Body(this, {x:4,y:7}));
	this.bodies.push(new Body(this, {x:8,y:7}));
	this.bodies.push(new Body(this, {x:9,y:7}));

	this.bodies.push(new Body(this, {x:3,y:8}));
	this.bodies.push(new Body(this, {x:4,y:8}));
	this.bodies.push(new Body(this, {x:8,y:8}));
	this.bodies.push(new Body(this, {x:9,y:8}));
	
	this.bodies.push(new Body(this, {x:3,y:9}));
	this.bodies.push(new Body(this, {x:4,y:9}));
	this.bodies.push(new Body(this, {x:8,y:9}));
	this.bodies.push(new Body(this, {x:9,y:9}));
	
	this.bodies.push(new Body(this, {x:3,y:10}));
	this.bodies.push(new Body(this, {x:4,y:10}));
	this.bodies.push(new Body(this, {x:5,y:10}));
	this.bodies.push(new Body(this, {x:6,y:10}));
	this.bodies.push(new Body(this, {x:7,y:10}));
	this.bodies.push(new Body(this, {x:8,y:10}));
	this.bodies.push(new Body(this, {x:9,y:10}));
	
	this.bodies.push(new Body(this, {x:3,y:11}));
	this.bodies.push(new Body(this, {x:4,y:11}));
	this.bodies.push(new Body(this, {x:5,y:11}));
	this.bodies.push(new Body(this, {x:6,y:11}));
	this.bodies.push(new Body(this, {x:7,y:11}));
	this.bodies.push(new Body(this, {x:8,y:11}));
	this.bodies.push(new Body(this, {x:9,y:11}));
	
	// doors
	this.bodies.push(new Body(this, {x:3,y:5, strength:25, width:0.1,height:1}));
	this.bodies.push(new Body(this, {x:7,y:6, strength:25, width:1,height:0.11}));
	
	// alarm
	this.alarms.push(new Alarm(this,{x:5,y:5}));
	
	// goal
	new Goal(this, {x:6,y:8});
	
	// Player
	this.player = new Player(this, {x:1, y:1});
};

/**
 * Initialise the bodies etc in the world
 */
Game.prototype.initWorld = function() {

	this.bodies = [];
	this.bombs = [];
	this.alarms = [];
	
	this.mapHeight = 50;
	this.mapWidth = 50;

	// mask bits for collision filtering
	this.world.masks = {}; 
	this.world.masks.PLAYER = 1;
	this.world.masks.BOMB = 2;
	this.world.masks.BLAST = 4;
	this.world.masks.BUILDING = 8;
	this.world.masks.ALARM = 16;
	this.world.masks.GOAL = 32;
	
	// Build world boundaries out of fixed walls
	for (var wide = 0; wide < this.mapWidth; wide++) {
		for (var high = 0; high < this.mapHeight; high++) {
			if (wide == 0 || wide == this.mapWidth-1 || high == 0 || high == this.mapHeight-1) {
				this.bodies.push(new Body(this, {x:wide,y:high,fixed:true}));
			}
		}
	}
	
};



/**
 * Initialise Box2D bits and pieces
 */
Game.prototype.initBox2D = function(canvas) {
	// Put Box2D stuff in global namespace
	b2Vec2 = Box2D.Common.Math.b2Vec2;
	b2BodyDef = Box2D.Dynamics.b2BodyDef;
	b2Body = Box2D.Dynamics.b2Body;
	b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
	b2Fixture = Box2D.Dynamics.b2Fixture;
	b2World = Box2D.Dynamics.b2World;
	b2MassData = Box2D.Collision.Shapes.b2MassData;
	b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
	b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
	b2DebugDraw = Box2D.Dynamics.b2DebugDraw;
	
	// Setup a top-down zero gravity world
	this.gravity = new b2Vec2(0,0);
	this.world = new b2World(this.gravity,true);

	// Scale of 32 pixels to the meter
	this.scale = 32;
	
	// Canvas context
    this.context = canvas.getContext("2d");
    
    // Timing defaults
    this.deltaRemaining = 0;
    this.stepAmount = 1/60;
    
    // setup collision listeners
    this.listener = new Box2D.Dynamics.b2ContactListener();
    this.listener.BeginContact = function(contact) {
    	var fixtureA = contact.GetFixtureA();
    	var fixtureB = contact.GetFixtureB();
    	
    	if (fixtureA && fixtureA.IsSensor()) {
    		var sensor = fixtureA.GetBody().GetUserData();
    		sensor.contact(contact);
    	}
    	if (fixtureB && fixtureB.IsSensor()) {
    		var sensor = fixtureB.GetBody().GetUserData();
    		sensor.contact(contact);
    	}
    	
    };
    this.listener.PostSolve = function(contact, impulse) {
        var bodyA = contact.GetFixtureA().GetBody().GetUserData();
        var bodyB = contact.GetFixtureB().GetBody().GetUserData();

        if (bodyA && bodyA.contact) {
            bodyA.contact(contact, impulse, true);
        }
        if (bodyB && bodyB.contact) {
            bodyB.contact(contact, impulse, false);
        }
        
    };
    this.world.SetContactListener(this.listener);
    
};

/**
 * Use debug drawing
 */
Game.prototype.debugDraw = function() {
	this.debugDraw = new b2DebugDraw();
	this.debugDraw.SetSprite(this.context);
	this.debugDraw.SetDrawScale(this.scale);
	this.debugDraw.SetFillAlpha(0.5);
	this.debugDraw.SetLineThickness(1.0);
	this.debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_joinBit);
	this.world.SetDebugDraw(this.debugDraw);
};

/**
 * Handle input
 */
Game.prototype.handleInput = function(event) {
	var x = 0, y=0;
	var velocity = this.game.player.body.GetLinearVelocity();

	if (event.keyCode == 87) {
		y -= 5;
	} else if (event.keyCode == 83) {
		y += 5;
	}
	
	if (event.keyCode == 65) {
		x -= 5;
	} else if (event.keyCode == 68) {
		x += 5;
	}

	if (event.keyCode == 81) {	//q
		this.game.player.plantBomb(this.game);
	}
	
	if (event.keyCode == 69) {	//e
		for (var i=0; i<this.game.bombs.length;i++) {
			this.game.bombs[i].explode();
		}
	}
	
	var velChangeX = x - velocity.x;
    var impulseX = this.game.player.body.GetMass() * velChangeX;
	var velChangeY = y - velocity.y;
    var impulseY = this.game.player.body.GetMass() * velChangeY;
    this.game.player.body.ApplyImpulse(new b2Vec2(impulseX,impulseY), 
    		this.game.player.body.GetWorldCenter() );

	
};

/**
 * Step the simulation
 */
Game.prototype.step = function(delta) {
	this.deltaRemaining += delta;
    while (this.deltaRemaining > this.stepAmount) {
        this.deltaRemaining -= this.stepAmount;
        this.world.Step(this.stepAmount, 8, 3); 
    }
    if (this.debugDraw) {
        this.world.DrawDebugData();
    }
    this.postStep();
};

/**
 * Handle post-step activities, e.g. destroying broken blocks
 */
Game.prototype.postStep = function() {
	
	// Update player
	this.player.update();
	
	this.bodies = this.bodies.filter(function(body){return body.update();});
	this.bombs = this.bombs.filter(function(bomb){return bomb.update();});
	this.alarms.forEach(function(e,i,a) {e.update();});

};
