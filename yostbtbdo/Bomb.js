/**
 * Create a new bomb
 * 
 * @param game Parent game
 * @param data {x||1, y||1, speed||100, power||100, rays||24}
 */
var Bomb = function(game, data) {
	this.game = game;
	this.data = data || {};
	this.world = this.game.world;
	this.speed = this.data.speed || 100;
	this.power = this.data.power || 100;
	this.rays = this.data.rays || 24;
	this.center = new b2Vec2(this.data.x || 1, this.data.y || 1);
	this.DEGTORAD = 0.0174532925199432957;
	
	// draw bomb on floor
	this.bodyDefinition = new b2BodyDef();
	this.fixtureDefinition = new b2FixtureDef();
	var circleShape = new b2CircleShape();
	circleShape.m_radius = 0.15;
	
	this.bodyDefinition.type = b2Body.b2_dynamicBody;
	this.bodyDefinition.position = this.center;
	this.fixtureDefinition.shape = circleShape;
	this.fixtureDefinition.filter.categoryBits = this.world.masks.BOMB;
	this.fixtureDefinition.filter.maskBits = 
		this.world.masks.ALARM | 
		this.world.masks.PLAYER;
	
	this.body = this.world.CreateBody(this.bodyDefinition);
	this.body.CreateFixture(this.fixtureDefinition);
	
	this.isExploded = false;
	this.isCleanedUp = false;
	
	// Let parent game know about this bomb
	this.game.bombs.push(this);
	
	// For cleaning up fragments
	this.cleanUpDelay = 500;
	this.fragments = [];
	
};

Bomb.prototype.explode = function() {
	// Based on example at http://www.iforce2d.net/b2dtut/explosions
	if (this.isExploded) {
		return;
	}
	this.isExploded = true;
	this.world.DestroyBody(this.body);
	
	for (var i=0; i < this.rays; i++) {
		
		var angle = (i/this.rays) * 360 * this.DEGTORAD;
		var rayDirection = new b2Vec2(Math.sin(angle) * this.speed, Math.cos(angle) * this.speed);
		
		var bodyDefinition = new b2BodyDef();
		var fixtureDefinition = new b2FixtureDef();
		var circleShape = new b2CircleShape();
		circleShape.m_radius = 0.05;
		
		bodyDefinition.type = b2Body.b2_dynamicBody;
		bodyDefinition.fixedRotation = true;
		bodyDefinition.bullet = true;
		bodyDefinition.linearDamping = 10;
		bodyDefinition.gravityScale = 0;
		bodyDefinition.position = this.center;
		bodyDefinition.linearVelocity = rayDirection;

		fixtureDefinition.shape = circleShape;
		fixtureDefinition.density = this.power/this.rays;
		fixtureDefinition.friction = 0;
		fixtureDefinition.restitution = 0.75;
		fixtureDefinition.filter.groupIndex = -1;
		fixtureDefinition.filter.categoryBits = this.world.masks.BLAST;
		fixtureDefinition.filter.maskBits = this.world.masks.ALARM | this.world.masks.PLAYER | this.world.masks.BUILDING;
		
		var body = this.world.CreateBody(bodyDefinition);
		body.CreateFixture(fixtureDefinition);
		this.fragments.push(body);
	}
	var that = this;
	setTimeout(function() {that.cleanUp();},this.cleanUpDelay);
	
};

/**
 * Clean up any of the explosion fragments
 */
Bomb.prototype.cleanUp = function() {
	for (var i=0; i<this.fragments.length;i++) {
		this.world.DestroyBody(this.fragments[i]);
	}
	this.isCleanedUp = true;
};

/**
 * Update the bomb
 */
Bomb.prototype.update = function() {
	if (this.isCleanedUp) {
		return false;
	} else {
		return true;
	}
};