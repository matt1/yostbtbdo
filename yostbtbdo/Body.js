/**
 * Create a new dynamic box
 * 
 * @param game parent game object
 * @param data {x, y, fixed||false, density, friction, restitution, width||1, height||1}
 * @param size {height, width}
 * @param fixture {density, friction, restitution}
 */
var Body = function(game, data) {
	this.data = data || {};
	this.position = this.data.position || {};
	this.fixture = this.data.fixture || {};
	this.width = this.data.width || 1;
	this.height = this.data.height || 1;
	this.game = game;
	this.world = this.game.world;
	
	this.isBroken = false;
	this.breakable = this.position.breakable || true;
	this.breakingMagnitude = this.position.strength || 100;
	
	this.bodyDefinition = new b2BodyDef();
	this.fixtureDefinition = new b2FixtureDef();
	
	// Apply defaults
	this.bodyDefinition.active = true;
	this.bodyDefinition.allowSleep = true;
	this.bodyDefinition.asleep = false;
	if (!this.data.fixed) {
		this.bodyDefinition.type = b2Body.b2_dynamicBody;
	} else {
		this.bodyDefinition.type = b2Body.b2_staticBody;
		this.breakable = false;
	}
	this.bodyDefinition.userData = this;
	this.bodyDefinition.linearDamping = 35;	// floor friction
	this.bodyDefinition.angularDamping = 25;	
	
	this.fixtureDefinition.density = this.data.density || 1;
	this.fixtureDefinition.friction = this.data.friction || 0.9;
	this.fixtureDefinition.restitution = this.data.restitution || 0.2;
	this.fixtureDefinition.shape = new b2PolygonShape();
	this.fixtureDefinition.filter.categoryBits = this.world.masks.BUILDING;
	this.fixtureDefinition.filter.maskBits = 
		this.world.masks.BLAST | 
		this.world.masks.PLAYER | 
		this.world.masks.BUILDING;

	this.fixtureDefinition.shape.SetAsBox(this.width/2 || 0.5, this.height/2 || 0.5, new b2Vec2(0,0),0);
	
	// Position
	this.bodyDefinition.position = new b2Vec2(this.data.x || 0, this.data.y || 0);
	
	// Create body & fixture in world
	this.body = this.world.CreateBody(this.bodyDefinition);
	this.body.CreateFixture(this.fixtureDefinition);
	
};

/**
 * Handle collisions for this object
 */
Body.prototype.contact = function(contact, impulse, first) {
    if (this.breakable){
		var magnitude =	Math.sqrt(
			impulse.normalImpulses[0] * 
			impulse.normalImpulses[0] + 
			impulse.normalImpulses[1] * 
			impulse.normalImpulses[1]);
	 
	    if (magnitude >= this.breakingMagnitude) {
	        this.isBroken = true;
	    } else {
	    	this.breakingMagnitude -= magnitude;
	    }
    }
};

/**
 * Update the body
 */
Body.prototype.update = function() {
	
	// Clean up dead bodies from simulation
	if (this.isBroken) {
		this.world.DestroyBody(this.body);
		return false;
	} else {
		return true;
	}
	
	
};

