/**
 * Creates a new player.
 * 
 * @param game Parent game
 * @param data {x||1,y||1}
 */
var Player = function(game, data) {
	this.bombs = 5;
	this.data = data || {};
	this.position = new b2Vec2(this.data.x || 1, this.data.y || 1);
	this.game = game;
	this.world = this.game.world;
	
	this.health = 2;
	
	this.bodyDefinition = new b2BodyDef();
	this.fixtureDefinition = new b2FixtureDef();
	var circleShape = new b2CircleShape();
	circleShape.m_radius = 0.25;
	
	this.bodyDefinition.type = b2Body.b2_dynamicBody;
	this.bodyDefinition.position = this.position;
	this.bodyDefinition.userData = this;
	this.bodyDefinition.linearDamping = 2;
	this.bodyDefinition.angularDamping = 2;
	
	this.fixtureDefinition.density = 0.01;
	this.fixtureDefinition.shape = circleShape;
	this.fixtureDefinition.filter.categoryBits = this.world.masks.PLAYER;
	this.fixtureDefinition.filter.maskBits = 
		this.world.masks.ALARM | 
		this.world.masks.BLAST | 
		this.world.masks.BUILDING | 
		this.world.masks.GOAL;
	
	this.body = this.world.CreateBody(this.bodyDefinition);
	this.body.CreateFixture(this.fixtureDefinition);
};

/**
 * Handle impacts for the player
 */
Player.prototype.contact = function(contact, impulse, first) {
	var magnitude =	Math.sqrt(
		impulse.normalImpulses[0] * 
		impulse.normalImpulses[0] + 
		impulse.normalImpulses[1] * 
		impulse.normalImpulses[1]);
 
    if (magnitude >= this.health) {
    	console.log("Player took too much damage and has died.");
        this.isDead = true;
    } else {
    	this.health -= magnitude;
    }
};

/**
 * Plant a bomb at this player's position
 */
Player.prototype.plantBomb = function() {
	this.bombs--;
	if (this.bombs == 0) {
		console.log("no bombs left");
		return;
	}
	new Bomb(this.game, {x:this.position.x,y:this.position.y});
};

/**
 * Update based on new location etc.
 */
Player.prototype.update = function() {
	this.position = this.body.GetWorldCenter();

};
